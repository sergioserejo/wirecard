Feature: Order

  Background:
    * def order = read('../archive/ExistingCustomerOrder.json')
    * def orderFail = read('../archive/NotExistingCustomerOrder.json')
    * def errorClientNotFound = read('../archive/ClientNotFoundError.json')
    * def createOrderWithCheckoutWirecard = read('../archive/ClientNotFoundError.json')
    * def searchByIdentifier = read('../archive/AnswerSearchByIdentifier.json')
    * def createOrderNewCustomer = read(karate.properties['user.dir'] + '/src/test/resources/archive/CreateOrderWithNewCustomer.json')
    * def baseUrl = 'https://sandbox.moip.com.br'
    * def uuid = function(max){ return Math.floor(Math.random() * max) }
    * def orderContract = read('../contracts/orderContract.json')
    * def customerContract = read('../contracts/customerContract.json')
    * def contractAddresses = read('../contracts/contractAddresses.json')
    * def contractAmount = read('../contracts/contractAmount.json')
    * def contractItems = read('../contracts/contractItems.json')
    * def contractNewCustomerForOrder = read('../contracts/contractNewCustomerForOrder.json')
    * configure headers = { Authorization: 'Basic RlNEQlNTUUdHRFY3V05TSTRUR1FZN1VDMlBCUFFXS1k6RTFNQUZVVEdFS0tOM1hZT0dTV0MzUERBTjlSSlNPMkQyTUJZVUZWNQ=='}

  Scenario: Existing Customer Order
    Given url baseUrl+'/v2/orders'
    When request order
    And method POST
    Then status 201
    Then match response contains orderContract
    And match response.amount == contractAmount
    And match response.items[0] == contractItems
    And match response.addresses[0] == contractAddresses
    And match response.customer contains customerContract

  Scenario: Not Existing Customer Order
    Given url baseUrl+'/v2/orders'
    When request orderFail
    And method POST
    Then status 400
    Then match response == errorClientNotFound

  Scenario: Create order with a new customer
    Given url baseUrl+'/v2/orders'
    When request createOrderNewCustomer
    And method POST
    Then status 201
    Then match response contains orderContract
    And match response.amount == contractAmount
    And match response.items[0] == contractItems
    And match response.customer contains contractNewCustomerForOrder

  Scenario: Consult Request By Identifier
    Given url baseUrl+'/v2/orders/ORD-H1CDZN4KMLCT'
    And method GET
    Then status 200
    Then match response == searchByIdentifier
    And match response contains orderContract
    And match response.amount == contractAmount
    And match response.items[0] == contractItems
    And match response.addresses[0] == contractAddresses
    And match response.customer contains customerContract