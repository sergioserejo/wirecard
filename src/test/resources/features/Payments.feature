Feature: Order

  Background:
    * def payment = read(karate.properties['user.dir'] + '/src/test/resources/archive/CreatePaymentWithCrcCode.json')
    * def paymentOnline = read(karate.properties['user.dir'] + '/src/test/resources/archive/CreateOnlineDebitPayment.json')
    * def paymentSlip = read(karate.properties['user.dir'] + '/src/test/resources/archive/CreatePaymentSlip.json')
    * def contractPaymentOnline = read('../contracts/contractPaymentOnline.json')
    * def contractPaymentCRC = read('../contracts/contractPaymentCRC.json')
    * def contractErrorsPayment = read('../contracts/contractErrorsPayment.json')
    * def baseUrl = 'https://sandbox.moip.com.br'
    * configure headers = { Authorization: 'Basic RlNEQlNTUUdHRFY3V05TSTRUR1FZN1VDMlBCUFFXS1k6RTFNQUZVVEdFS0tOM1hZT0dTV0MzUERBTjlSSlNPMkQyTUJZVUZWNQ=='}

  Scenario: Create a payment with CRC code
    * def order = call read('ExistingCustomerOrder.feature')
    * def idOrder = order.response.id
    Given url baseUrl+'/v2/orders/'+idOrder+'/payments'
    When request payment
    And set payment.fundingInstrument.creditCard.id = 'CRC-KN6LCXU0SCUW'
    And method POST
    Then status 201
    And match response contains contractPaymentCRC

  Scenario: Create online debit payment
    * def order = call read('ExistingCustomerOrder.feature')
    * def idOrder = order.response.id
    Given url baseUrl+'/v2/orders/'+idOrder+'/payments'
    When request paymentOnline
    And set paymentOnline.fundingInstrument.onlineBankDebit.expirationDate = '2021-11-22'
    And method POST
    Then status 201
    And match response contains contractPaymentOnline

  Scenario: Create online debit payment ( Expiration date is less than current )
    * def order = call read('ExistingCustomerOrder.feature')
    * def idOrder = order.response.id
    Given url baseUrl+'/v2/orders/'+idOrder+'/payments'
    When request paymentOnline
    And method POST
    Then status 400
    And match response.errors[0].description ==  'A data de expiração do boleto bancário é anterior a atual'
    And match response == contractErrorsPayment

  Scenario: Create payment slip ( Inconsistency in the CPF / CNPJ of the payer )
    * def order = call read('ExistingCustomerOrder.feature')
    * def idOrder = order.response.id
    Given url baseUrl+'/v2/orders/'+idOrder+'/payments'
    When request paymentSlip
    And set paymentSlip.fundingInstrument.boleto.expirationDate = '2021-11-22'
    And method POST
    Then status 400
    And match response.errors[0].description ==  'Verifique o CPF/CNPJ do pagador'
    And match response == contractErrorsPayment

  Scenario: Create payment slip ( Expiration date is less than current )
    * def order = call read('ExistingCustomerOrder.feature')
    * def idOrder = order.response.id
    Given url baseUrl+'/v2/orders/'+idOrder+'/payments'
    When request paymentSlip
    And method POST
    Then status 400
    And match response.errors[0].description ==  'A data de expiração do boleto bancário é anterior a atual'
    And match response == contractErrorsPayment