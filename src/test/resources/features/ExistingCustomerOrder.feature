Feature: Existing Customer Order

  Background:
    * def order = read('../archive/ExistingCustomerOrder.json')
    * def baseUrl = 'https://sandbox.moip.com.br'
    * configure headers = { Authorization: 'Basic RlNEQlNTUUdHRFY3V05TSTRUR1FZN1VDMlBCUFFXS1k6RTFNQUZVVEdFS0tOM1hZT0dTV0MzUERBTjlSSlNPMkQyTUJZVUZWNQ=='}

  Scenario: Existing Customer Order
    Given url baseUrl+'/v2/orders'
    When request order
    And method POST
    Then status 201