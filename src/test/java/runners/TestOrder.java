package runners;

import com.intuit.karate.junit4.Karate;
import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Karate.class)
@CucumberOptions(
        features = "src/test/resources/features/Orders.feature",
        plugin = {"pretty","html:target/report-html","json:target/report.json"})
public class TestOrder {
}
