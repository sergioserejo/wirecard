# Tecnologia

Foi usado:
- Karate Framework

# Referência do Karate Framework
[Clique aqui](https://github.com/intuit/karate) para ver tudo sobre o Karate Framework.

# CI/CD
No projeto foi adicionado CI/CD onde temos dois stages ( build e test ), para executar o pipeline [clique aqui](https://gitlab.com/sergioserejo/wirecard/-/pipelines) e selecione Run Pipeline.
